# riak_extract
Extraction of riak database into csv format using riak_pb application

# Usage
riak_extract_operations:walk_db("/var/lib/riak/release/riak_files/riak_export.csv", all, all, 200, 200000, 300000).

riak_extract_operations:live_hack(Keys, "/home/sghatak/pdf/riak/riak_if/extracted_files/riak_export.csv", ['BUS_5GB'], all, 4).

riak_extract_operations:live_hack(A, "/var/lib/riak/release/riak_files/riak_export.csv", all, all, 100).

riak_extract_operations:read_and_extract("/home/sghatak/pdf/riak/riak_if/msisdns", "/home/sghatak/pdf/riak/riak_if/extracted_files/riak_file_export.csv",4,10).

riak_extract_operations:read_and_extract("/var/lib/riak/release/msisdns", "/var/lib/riak/release/riak_files/riak_file_export.csv",200,200000).
