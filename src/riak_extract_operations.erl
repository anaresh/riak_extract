%%-------------------------------------------------------------------
%% @author Naresh Aniyaliya <naresh.aniyaliya@ee.co.uk>
%% @copyright 2016 Everything Everywhere Limited
%% @version 0.1
%% @doc 
%% Module to extract riak data into csv format.
%% @end
%%-------------------------------------------------------------------

-module(riak_extract_operations).

-compile(export_all).

%%-------------------------------------------------------------------
%% Do walk db over riak and extract
%%-------------------------------------------------------------------
walk_db(File, CTypes, AllOrActive) ->
    walk_db(File, CTypes, AllOrActive, 4, 200000, 36000000).

walk_db(File, CTypes, AllOrActive, WPoolsize) ->
    walk_db(File, CTypes, AllOrActive, WPoolsize, 200000, 36000000).

walk_db(File, CTypes, AllOrActive, WPoolsize, EntryRotate) ->
    walk_db(File, CTypes, AllOrActive, WPoolsize, EntryRotate, 36000000).

walk_db(File, CTypes, AllOrActive, WPoolsize, EntryRotate, Timeout) ->
    riak_walk_db:walk_db(File, CTypes, AllOrActive, WPoolsize, EntryRotate, Timeout).

live_hack(Key_list, File, CTypes, AllOrActive, WPoolsize) ->
    live_hack(Key_list, File, CTypes, AllOrActive, WPoolsize, 200000).

live_hack(Key_list, File, CTypes, AllOrActive, WPoolsize, EntryRotate) ->
    riak_walk_db:live_hack(Key_list, File, CTypes, AllOrActive, WPoolsize, EntryRotate).

%%-------------------------------------------------------------------
%% read msisdns from file provided by huawei and extract
%%-------------------------------------------------------------------
read_and_extract(File, OutFile) ->
    read_and_extract(File, OutFile, 4, 200000).

read_and_extract(File, OutFile, WPoolsize, EntryRotate) ->
    from_file:read_and_extract(File, OutFile, WPoolsize, EntryRotate).
