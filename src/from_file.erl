%%-------------------------------------------------------------------
%% @author Naresh Aniyaliya <naresh.aniyaliya@ee.co.uk>
%% @copyright 2016 Everything Everywhere Limited
%% @version 0.1
%% @doc 
%% Module to extract riak data into csv format.
%% @end
%%-------------------------------------------------------------------

-module(from_file).

-include_lib("ebdc_eedb/include/eedb.hrl").
-include_lib("ebdc_db/include/ebdc_db.hrl").

-compile(export_all).

read_and_extract(File, OutFile, WPoolsize, EntryRotate) ->
    ebdc_log_util:syslog_msg("Start time of extraction :: ~p~n", [{date(),time()}]),
    case  catch file:consult(File) of
        {ok, [Key_list]} ->
            Pid = spawn(?MODULE, process, [Key_list, OutFile, WPoolsize, EntryRotate]),
            io:format("started master process ~p~n",[Pid]),
            register(master_rd_process, Pid);
        Error ->
            io:format("Error !!!! while reading from ~p : ~p~n",[File, Error])
    end.

process(Key_list, Out_file, WPoolsize, EntryRotate) ->
    io:format("calculating size of the extract per thread...~n"),
    Len = length(Key_list),
    Size = round(Len/WPoolsize),
    io:format("calculated !! size for each thread would be ~p~n",[Size]),
    start_workers(Key_list, Out_file, WPoolsize, EntryRotate, Size),
    wait_for_workers(lists:seq(1, WPoolsize)).

start_workers(Key_list, File, 0, EntryRotate, _) ->
    Pid = spawn(?MODULE, worker_loop, [Key_list, File, 0, EntryRotate]),
    register(worker_name(0), Pid),
    io:format("[Master] : ~p worker(last) started with ProcessID = ~p ~n",[0, Pid]); 
start_workers(Key_list, File, N, EntryRotate, Size) ->
    case catch lists:split(Size, Key_list) of
        {'EXIT',{badarg, _Error}} ->
            Pid = spawn(?MODULE, worker_loop, [Key_list, File, N, EntryRotate]),
            register(worker_name(N), Pid),
            io:format("[Master] : ~p worker(last) started with ProcessID = ~p ~n",[N, Pid]), 
            io:format("[Master] : Last process has ~p msisdns to etract~n",[length(Key_list)]);
        {SubList, Rest} ->
            Pid = spawn(?MODULE, worker_loop, [SubList, File, N, EntryRotate]),
            register(worker_name(N), Pid),
            io:format("[Master] : ~p worker started with ProcessID = ~p ~n",[N, Pid]),
            start_workers(Rest, File, N-1, EntryRotate, Size)
    end.

worker_loop(KeyList, File, Own_number, EntryRotate) ->
    {ok, Out} = file:open(fname(File, Own_number), [write]),
    worker_loop(KeyList, File, Own_number, EntryRotate, Out, 0).

worker_loop([], _File, Own_number, _EntryRotate, Out, _Count) ->
    file:close(Out),
    io:format("[~p] : done execution~n",[worker_name(Own_number)]),
    whereis(master_rd_process) ! {worker_done, Own_number},
    ok;
worker_loop([Key | Rest], File, Own_number, EntryRotate, Out, Count) ->
    try
        {New_out, New_count} =
            case Count > EntryRotate of
                true ->
	            file:close(Out),
	            {ok, NOut} = file:open(fname(File, Own_number), [write]),
	            {NOut, 0};
	        false ->
	            {Out, Count}
            end,
        Count_1 = read_active_passes(Key, New_out, New_count),
        Count_2 = read_expired_passes(Key, New_out, Count_1),
        worker_loop(Rest, File, Own_number, EntryRotate, New_out, Count_2)
    catch ERR1:ERR2 ->
        io:format("~n~nEXIT: ~p ~p ~p~n~p~n~n",
	    [Key, ERR1,ERR2,erlang:get_stacktrace()]),
        worker_loop(Rest, File, Own_number, EntryRotate, Out, Count)
    end.


read_active_passes(Key, Out_fd, Count) ->
    case catch ebdc_eedb_cache:lookup(Key) of
        {ok, #share_data{passes = Passes}} ->
            case catch ebdc_eedb_volume_used:lookup_info(Key) of
                {#volume_record{pass_records = VolRecords}, _, _} ->
                    Extracted_passes = extract_pass_information(Passes, VolRecords, []),
                    [ write_to_csv(active_pass, Out_fd, Key, X) || X<-Extracted_passes],
                    Count+length(Extracted_passes);
                Error ->
                    log("Riak read of Volume_rec failed for ~p : Reason ~p~n",[Key, Error]),
                    Count
            end;
        Error ->
            log("Riak read of active_pass failed for ~p : Reason ~p~n",[Key, Error]),
            Count
    end.

read_expired_passes(Key, Out_fd, Count) ->
    case catch ebdc_eedb_expired_pass:lookup_info(Key) of
        {EPassList, _, _} ->
            EPasses = extract_expired_pass_info(EPassList, []),
            [ write_to_csv(expired_pass, Out_fd, Key, X) || X<-EPasses],
            Count+length(EPasses);
        Error ->
            log("Riak read of expired_pass failed for ~p : Reason ~p~n",[Key, Error]),
            Count
    end.

extract_expired_pass_info([], Acc) -> Acc;
extract_expired_pass_info([{_, #expired_pass3{key           = {_, Template_id,_},
                                              purchase_ts   = Start_ts,
                                              expiry_reason = Exp_reason,
                                              expired_ts    = End_ts,
                                              sales_channel = Channel}} |T], Acc) ->
    extract_expired_pass_info(T, [{Template_id, Start_ts, End_ts, Exp_reason, Channel}|Acc]);
extract_expired_pass_info([ Default | T], Acc) ->
    log("Default case of expired pass ~p~n", [Default]),    
    extract_expired_pass_info(T, Acc).

extract_pass_information([], _VolRecords, Acc) -> Acc;
extract_pass_information([{#subse_pass{template_id = PassID, 
                                       uniq_id     = UniqID,
                                       start_ts    = Start_ts, 
                                       end_ts      = End_ts}, _} | T], VolRecords, Acc) ->
    Vol = 
        case lists:keyfind({PassID,UniqID}, #pass_record.key, VolRecords) of
	    #pass_record{total = {_,total,Used,_,_}} -> Used;
            _ -> 0
	end,
    extract_pass_information(T, VolRecords, [{PassID, convert(Start_ts), convert(End_ts), Vol}|Acc]);
extract_pass_information([Default | T], VolRecords, Acc) ->
    log("Default case of active pass ~p~n", [Default]),
    extract_pass_information(T, VolRecords, Acc).

    
convert({{_,_,_},{_,_,_}} = DateTime) ->
    calendar:datetime_to_gregorian_seconds(DateTime);
convert(Anything) ->
    Anything.

worker_name(N) ->
    list_to_atom("ee_worker_" ++ integer_to_list(N)).

cleanup_workers() -> cleanup_workers(4).
cleanup_workers(0) -> 
    ebdc_log_util:syslog_msg("Cleanedup time of Extraction :: ~p~n",[{date(),time()}]),
    ok;
cleanup_workers(N) ->
    catch exit(whereis(worker_name(N)), kill),
    cleanup_workers(N-1).

check_workers() -> check_workers(4).
check_workers(N) -> check_workers(N, []).
check_workers(0, Ret) -> 
    io:format("~p~n",[lists:reverse(Ret)]);
check_workers(N, Acc) ->
    check_workers(N-1, [whereis(worker_name(N))|Acc]).

fname(File, N) ->
    {MegI, SecI, MilI} = os:timestamp(),
    NStr = integer_to_list(N),
    MegS = integer_to_list(MegI),
    SecS = integer_to_list(SecI),
    MilS = integer_to_list(MilI),
    File ++ "." ++ NStr ++ "." ++ MegS ++ "." ++ SecS ++ "." ++ MilS.

wait_for_workers([]) -> 
    ebdc_log_util:syslog_msg("End time of Extraction :: ~p~n",[{date(),time()}]),
    ok;
wait_for_workers(Acc) ->
    receive
	{worker_done, N} ->
            io:format("[master] : still waiting for ~p workers~n",[lists:delete(N, Acc)]), 
            wait_for_workers(lists:delete(N, Acc))
    end.

log(Message, List) ->
    case config:get_env(ebdc_eedb, should_log_error, false) of
        true -> ebdc_log_util:syslog_msg(Message, List);
        _    -> ok
    end.


%write_to_csv(Tag, _Out, Key, Data) ->
%    io:format("~p,~p,~p~n",[Tag,Key,Data]).

write_to_csv(expired_pass, Out_fd, Key, {EPassID, STS, ETS, ERsn, Channel}) ->
    file:write(Out_fd,
        io_lib:format(
	    "~nexpired_pass,~p,~p,~p,~p,~p,~p",
	    [Key,EPassID,STS,ETS,ERsn,Channel]));
write_to_csv(active_pass, Out_fd, Key, {PassID,STS,ETS,Vol}) ->
    file:write(Out_fd,
        io_lib:format(
            "~nactive_pass,~p,~p,~p,~p,~p",
            [Key,PassID,STS,ETS,Vol])).

