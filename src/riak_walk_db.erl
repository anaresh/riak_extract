%%-------------------------------------------------------------------
%% @author Naresh Aniyaliya <naresh.aniyaliya@ee.co.uk>
%% @copyright 2016 Everything Everywhere Limited
%% @version 0.1
%% @doc 
%% Module to extract riak data into csv format.
%% @end
%%-------------------------------------------------------------------

-module(riak_walk_db).

-include_lib("ebdc_eedb/include/eedb.hrl").
-include_lib("ebdc_db/include/ebdc_db.hrl").

-compile(export_all).

walk_db(File, CTypes, AllOrActive, WPoolsize, EntryRotate, Timeout) ->
    ebdc_log_util:syslog_msg("Start time of extraction :: ~p~n", [{date(),time()}]),
    Pid = spawn(?MODULE, process, [CTypes, AllOrActive, File, WPoolsize, EntryRotate, Timeout]),
    io:format("started master process ~p~n",[Pid]),
    register(master_rd_process, Pid).

live_hack(Key_list, File, CTypes, AllOrActive, WPoolsize, EntryRotate) ->
    Pid = spawn(?MODULE, process1, [Key_list, CTypes, AllOrActive, File, WPoolsize, EntryRotate]),
    io:format("started master process ~p~n",[Pid]),
    register(master_rd_process, Pid).

process1(Key_list, CTypes, AllOrActive, File, WPoolsize, EntryRotate) ->
    io:format("calculating size of the extract per thread...~n"),
    Len = length(Key_list),
    Size = round(Len/WPoolsize),
    io:format("calculated !! size for each thread would be ~p~n",[Size]),
    start_workers(Key_list, CTypes, AllOrActive, File, WPoolsize, EntryRotate, Size),
    wait_for_workers(lists:seq(1, WPoolsize)).

process(CTypes, AllOrActive, File, WPoolsize, EntryRotate, Timeout) ->
    io:format("Fetching keys from riak...~n"),
    case get_keys(3, Timeout) of
        {ok, Key_list} ->
            Len = length(Key_list),
            Size = round(Len/WPoolsize),
            io:format("calculated size for each thread would be ~p~n",[Size]),
            start_workers(Key_list, CTypes, AllOrActive, File, WPoolsize, EntryRotate, Size),
            wait_for_workers(lists:seq(1, WPoolsize));
        _Error ->
            not_able_to_read_keys
    end.

get_keys(0, _Timeout) ->
    not_found;
get_keys(Try, Timeout) ->
    Conn = eedb_connection_pool:get_conn(),
    case riakc_pb_socket:list_keys(Conn, <<"user_data_cache">>, [{timeout,Timeout}]) of
        {ok, Key_list} -> 
            io:format("Completed !! Fetched keys from riak...~n"),
            {ok, Key_list};
        Error ->
            io:format("### Reading keys. Try ~p failed with ~n~p~n",[Try, Error]),
            get_keys(Try-1, Timeout)
    end.

start_workers(Key_list, Ct, Aorctive, File, 0, EntryRotate, _) ->
    Pid = spawn(?MODULE, worker_loop, [Key_list, Ct, Aorctive, File, 0, EntryRotate]),
    register(worker_name(0), Pid),
    io:format("[Master] : ~p worker(last) started with ProcessID = ~p ~n",[0, Pid]); 
start_workers(Key_list, Ct, Aorctive, File, N, EntryRotate, Size) ->
    case catch lists:split(Size, Key_list) of
        {'EXIT',{badarg, _Error}} ->
            Pid = spawn(?MODULE, worker_loop, [Key_list, Ct, Aorctive, File, N, EntryRotate]),
            register(worker_name(N), Pid),
            io:format("[Master] : ~p worker(last) started with ProcessID = ~p ~n",[N, Pid]), 
            io:format("[Master] : Last process has ~p msisdns to etract~n",[length(Key_list)]);
        {SubList, Rest} ->
            Pid = spawn(?MODULE, worker_loop, [SubList, Ct, Aorctive, File, N, EntryRotate]),
            register(worker_name(N), Pid),
            io:format("[Master] : ~p worker started with ProcessID = ~p ~n",[N, Pid]),
            start_workers(Rest, Ct, Aorctive, File, N-1, EntryRotate, Size)
    end.

worker_loop(KeyList, Cust_types, AllOrActive, File, Own_number, EntryRotate) ->
    {ok, Out} = file:open(fname(File, Own_number), [write]),
    worker_loop(KeyList, Cust_types, AllOrActive, File, Own_number, EntryRotate, Out, 0).

worker_loop([], _Cust_types, _AllOrActive, _File, Own_number, _EntryRotate, Out, _Count) ->
    file:close(Out),
    io:format("[~p] : done execution~n",[worker_name(Own_number)]),
    whereis(master_rd_process) ! {worker_done, Own_number},
    ok;
worker_loop([First | Rest], Cust_types, AllOrActive, File, Own_number, EntryRotate, Out, Count) ->
    Key = erlang:binary_to_term(First),
    try
        {New_out, New_count} =
            case Count > EntryRotate of
                true ->
	            file:close(Out),
	            {ok, NOut} = file:open(fname(File, Own_number), [write]),
	            {NOut, 0};
	        false ->
	            {Out, Count}
            end,
        {Is_ext, Count_1} = read_active_passes(Key, Cust_types, New_out, New_count),
        Count_2 =
            case Is_ext == true andalso AllOrActive == all of
                true ->
                    read_expired_passes(Key, New_out, Count_1);
                false ->
                    Count_1
            end,
        worker_loop(Rest, Cust_types, AllOrActive, File, Own_number, EntryRotate, New_out, Count_2)
    catch ERR1:ERR2 ->
        io:format("~n~nEXIT: ~p ~p ~p~n~p~n~n",
	    [Key, ERR1,ERR2,erlang:get_stacktrace()]),
        worker_loop(Rest, Cust_types, AllOrActive, File, Own_number, EntryRotate, Out, Count)
    end.


read_active_passes(Key, Cust_types, Out_fd, Count) ->
    case catch ebdc_eedb_cache:lookup(Key) of
        {ok, #share_data{passes = Passes}} ->
            Cust_type_list = get_cust_type(Passes, []),
            case lists:member(true, member(Cust_type_list, Cust_types)) of
                true ->
                    case catch ebdc_eedb_volume_used:lookup_info(Key) of
                        {#volume_record{pass_records = VolRecords}, _, _} ->
                            Extracted_passes = extract_pass_information(Passes, VolRecords, []),
                            [ write_to_csv(active_pass, Out_fd, Key, X) || X<-Extracted_passes],
                            {true, Count+length(Extracted_passes)};
                        Error ->
                            log("Error while reading Volume record ~p~n",[Error]),
                            {false, Count}
                    end;
                _->
                    {false, Count}
            end;
        Error ->
            log("Error while reading active_pass ~p~n",[Error]),
            {false, Count}
    end.

read_expired_passes(Key, Out_fd, Count) ->
    case catch ebdc_eedb_expired_pass:lookup_info(Key) of
        {EPassList, _, _} ->
            EPasses = extract_expired_pass_info(EPassList, []),
            [ write_to_csv(expired_pass, Out_fd, Key, X) || X<-EPasses],
            Count+length(EPasses);
        Error ->
            log("Error while reading expired_pass ~p~n",[Error]),
            Count
    end.

extract_expired_pass_info([], Acc) -> Acc;
extract_expired_pass_info([{_, #expired_pass3{key           = {_, Template_id,_},
                                              purchase_ts   = Start_ts,
                                              expiry_reason = Exp_reason,
                                              expired_ts    = End_ts,
                                              sales_channel = Channel}} |T], Acc) ->
    extract_expired_pass_info(T, [{Template_id, Start_ts, End_ts, Exp_reason, Channel}|Acc]);
extract_expired_pass_info([ Default | T], Acc) ->
    log("Default case of expired pass ~p~n", [Default]),    
    extract_expired_pass_info(T, Acc).

extract_pass_information([], _VolRecords, Acc) -> Acc;
extract_pass_information([{#subse_pass{template_id = PassID, 
                                       uniq_id     = UniqID,
                                       start_ts    = Start_ts, 
                                       end_ts      = End_ts}, _} | T], VolRecords, Acc) ->
    Vol = 
        case lists:keyfind({PassID,UniqID}, #pass_record.key, VolRecords) of
	    #pass_record{total = {_,total,Used,_,_}} -> Used;
            _ -> 0
	end,
    extract_pass_information(T, VolRecords, [{PassID, convert(Start_ts), convert(End_ts), Vol}|Acc]);
extract_pass_information([Default | T], VolRecords, Acc) ->
    log("Default case of active pass ~p~n", [Default]),
    extract_pass_information(T, VolRecords, Acc).

get_cust_type([], Acc) -> Acc;
get_cust_type([{#subse_pass{cust_type=Cust_type}, _} | T], Acc) ->
    get_cust_type(T, [Cust_type|Acc]);
get_cust_type([_ |T], Acc) ->
    get_cust_type(T, Acc).
    
convert({{_,_,_},{_,_,_}} = DateTime) ->
    calendar:datetime_to_gregorian_seconds(DateTime);
convert(Anything) ->
    Anything.

member(_CType, all) -> [true];
member(CType_list, CTypes) -> [lists:member(X, CTypes) || X<-CType_list].

worker_name(N) ->
    list_to_atom("ee_worker_" ++ integer_to_list(N)).

cleanup_workers() -> cleanup_workers(4).
cleanup_workers(0) -> ok;
cleanup_workers(N) ->
    catch exit(whereis(worker_name(N)), kill),
    cleanup_workers(N-1).

check_workers() -> check_workers(4).
check_workers(N) -> check_workers(N, []).
check_workers(0, Ret) -> 
    io:format("~p~n",[lists:reverse(Ret)]);
check_workers(N, Acc) ->
    check_workers(N-1, [whereis(worker_name(N))|Acc]).

fname(File, N) ->
    {MegI, SecI, MilI} = os:timestamp(),
    NStr = integer_to_list(N),
    MegS = integer_to_list(MegI),
    SecS = integer_to_list(SecI),
    MilS = integer_to_list(MilI),
    File ++ "." ++ NStr ++ "." ++ MegS ++ "." ++ SecS ++ "." ++ MilS.

wait_for_workers([]) -> 
    ebdc_log_util:syslog_msg("End time of Extraction :: ~p~n",[{date(),time()}]),   
    ok;
wait_for_workers(Acc) ->
    receive
	{worker_done, N} ->
            io:format("[master] : still waiting for ~p workers~n",[lists:delete(N, Acc)]), 
            wait_for_workers(lists:delete(N, Acc))
    end.

log(Message, List) ->
    case config:get_env(ebdc_eedb, should_log_error, false) of
        true -> ebdc_log_util:syslog_msg(Message, List);
        _    -> ok
    end.


%write_to_csv(Tag, _Out, Key, Data) ->
%    io:format("~p,~p,~p~n",[Tag,Key,Data]).

write_to_csv(expired_pass, Out_fd, Key, {EPassID, STS, ETS, ERsn, Channel}) ->
    file:write(Out_fd,
        io_lib:format(
	    "~nexpired_pass,~p,~p,~p,~p,~p,~p",
	    [Key,EPassID,STS,ETS,ERsn,Channel]));
write_to_csv(active_pass, Out_fd, Key, {PassID,STS,ETS,Vol}) ->
    file:write(Out_fd,
        io_lib:format(
            "~nactive_pass,~p,~p,~p,~p,~p",
            [Key,PassID,STS,ETS,Vol])).

